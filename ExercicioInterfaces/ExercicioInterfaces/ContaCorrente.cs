﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioInterfaces
{
    public class ContaCorrente : IConta
    {
        private double Saldo { get; set; } 
        private double Taxa { get; set; }

        public void depositar(double valor)
        {
            Saldo = Saldo + valor;
        }

        public double getSaldo()
        {
            return Saldo;
        }

        public void levantar(double valor)
        {
            if (Saldo - valor - (valor * (Taxa / 100)) > 0)
                  Saldo = Saldo - valor - (valor * (Taxa / 100));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioInterfaces
{
    public class ContaPoupanca : IConta
    {
        public double Saldo;
        public void depositar(double valor)
        {
            Saldo = Saldo + valor;
        }


        public double getSaldo()
        {
            return Saldo;
        }

        public void levantar(double valor)
        {
            if (Saldo - valor >= 0 )
                 Saldo -=valor;
        }
    }
}

﻿namespace ExercicioInterfaces
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbContaCorrente = new System.Windows.Forms.RadioButton();
            this.rbContaPoupanca = new System.Windows.Forms.RadioButton();
            this.TextBoxValor = new System.Windows.Forms.TextBox();
            this.rbLevantar = new System.Windows.Forms.RadioButton();
            this.rbDepositar = new System.Windows.Forms.RadioButton();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LabelSaldo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbContaCorrente
            // 
            this.rbContaCorrente.AutoSize = true;
            this.rbContaCorrente.Checked = true;
            this.rbContaCorrente.Location = new System.Drawing.Point(6, 19);
            this.rbContaCorrente.Name = "rbContaCorrente";
            this.rbContaCorrente.Size = new System.Drawing.Size(96, 17);
            this.rbContaCorrente.TabIndex = 0;
            this.rbContaCorrente.TabStop = true;
            this.rbContaCorrente.Text = "Conta Corrente";
            this.rbContaCorrente.UseVisualStyleBackColor = true;
            this.rbContaCorrente.CheckedChanged += new System.EventHandler(this.rbContaCorrente_CheckedChanged);
            // 
            // rbContaPoupanca
            // 
            this.rbContaPoupanca.AutoSize = true;
            this.rbContaPoupanca.Location = new System.Drawing.Point(6, 60);
            this.rbContaPoupanca.Name = "rbContaPoupanca";
            this.rbContaPoupanca.Size = new System.Drawing.Size(105, 17);
            this.rbContaPoupanca.TabIndex = 1;
            this.rbContaPoupanca.Text = "Conta Poupança";
            this.rbContaPoupanca.UseVisualStyleBackColor = true;
            this.rbContaPoupanca.CheckedChanged += new System.EventHandler(this.rbContaPoupanca_CheckedChanged);
            // 
            // TextBoxValor
            // 
            this.TextBoxValor.Location = new System.Drawing.Point(262, 137);
            this.TextBoxValor.Name = "TextBoxValor";
            this.TextBoxValor.Size = new System.Drawing.Size(174, 20);
            this.TextBoxValor.TabIndex = 2;
            // 
            // rbLevantar
            // 
            this.rbLevantar.AutoSize = true;
            this.rbLevantar.Checked = true;
            this.rbLevantar.Location = new System.Drawing.Point(21, 19);
            this.rbLevantar.Name = "rbLevantar";
            this.rbLevantar.Size = new System.Drawing.Size(67, 17);
            this.rbLevantar.TabIndex = 3;
            this.rbLevantar.TabStop = true;
            this.rbLevantar.Text = "Levantar";
            this.rbLevantar.UseVisualStyleBackColor = true;
            // 
            // rbDepositar
            // 
            this.rbDepositar.AutoSize = true;
            this.rbDepositar.Location = new System.Drawing.Point(21, 60);
            this.rbDepositar.Name = "rbDepositar";
            this.rbDepositar.Size = new System.Drawing.Size(70, 17);
            this.rbDepositar.TabIndex = 4;
            this.rbDepositar.Text = "Depositar";
            this.rbDepositar.UseVisualStyleBackColor = true;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Location = new System.Drawing.Point(262, 180);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmar.TabIndex = 5;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbContaCorrente);
            this.groupBox1.Controls.Add(this.rbContaPoupanca);
            this.groupBox1.Location = new System.Drawing.Point(38, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(139, 88);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbLevantar);
            this.groupBox2.Controls.Add(this.rbDepositar);
            this.groupBox2.Location = new System.Drawing.Point(262, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 88);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // LabelSaldo
            // 
            this.LabelSaldo.AutoSize = true;
            this.LabelSaldo.Location = new System.Drawing.Point(66, 159);
            this.LabelSaldo.Name = "LabelSaldo";
            this.LabelSaldo.Size = new System.Drawing.Size(34, 13);
            this.LabelSaldo.TabIndex = 8;
            this.LabelSaldo.Text = "Saldo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Saldo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 229);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LabelSaldo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.TextBoxValor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbContaCorrente;
        private System.Windows.Forms.RadioButton rbContaPoupanca;
        private System.Windows.Forms.TextBox TextBoxValor;
        private System.Windows.Forms.RadioButton rbLevantar;
        private System.Windows.Forms.RadioButton rbDepositar;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label LabelSaldo;
        private System.Windows.Forms.Label label1;
    }
}


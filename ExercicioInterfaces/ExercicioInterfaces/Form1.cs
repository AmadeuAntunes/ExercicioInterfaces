﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExercicioInterfaces
{
    public partial class Form1 : Form
    {
        ContaCorrente cc = new ContaCorrente();
        ContaPoupanca cp = new ContaPoupanca();
        IConta c;
        public Form1()
        {
            InitializeComponent();
            LabelSaldo.Text = "0";
            TextBoxValor.Text = "0";
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
           
            if (rbContaCorrente.Checked)
            {
                c = cc;
            }
            else
            {
                c = cp;
            }

            if (rbDepositar.Checked)
            {
                c.depositar(Double.Parse(TextBoxValor.Text));
            }
            else
            {
                c.levantar(Double.Parse(TextBoxValor.Text));
            }
            LabelSaldo.Text = c.getSaldo().ToString();

        }

        private void rbContaCorrente_CheckedChanged(object sender, EventArgs e)
        {

            LabelSaldo.Text = cc.getSaldo().ToString();
        }

        private void rbContaPoupanca_CheckedChanged(object sender, EventArgs e)
        {
            LabelSaldo.Text = cp.getSaldo().ToString();
        }
    }
}

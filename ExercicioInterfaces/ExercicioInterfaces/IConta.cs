﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioInterfaces
{
    public interface IConta
    {
        void depositar(double valor);
        void levantar(double valor);
        double getSaldo();
    }
}
